package com.vbote.umivale.client.model;

import org.jboss.errai.common.client.api.annotations.Portable;
import org.jboss.errai.databinding.client.api.Bindable;

@Bindable
public class WelcomeInfo {
	private String message;
	private String id;
	
	public WelcomeInfo()
	{
		id = String.valueOf(System.currentTimeMillis());
	}

	public String getMessage() {
		return  message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
