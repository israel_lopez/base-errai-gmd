package com.vbote.umivale.client;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.jboss.errai.ui.nav.client.local.Page;
import org.jboss.errai.ui.nav.client.local.PageShown;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.vbote.umivale.client.events.PageChangeEvent;
import com.vbote.umivale.client.services.PersistenceManager;

import gwt.material.design.jquery.client.api.JQueryElement;

import static gwt.material.design.jquery.client.api.JQuery.$;

public abstract class BasePresenter implements IsWidget {

	String pageId;

	@Inject
	private Event<PageChangeEvent> pageChangeEvent;
	
	@Inject
	protected PersistenceManager persistenceManager; 

	protected BasePresenter ()
	{
		setPageId(this.getClass().getSimpleName());
	}
	@PostConstruct
	protected void onPostConstruct() {
 	
		pageChangeEvent.fire(new PageChangeEvent(getPageId()));

	}

	@PageShown
	protected void onShown() {
		$(".code i").on("click", e -> {
			JQueryElement el = $(e.getCurrentTarget()).parent().find("pre");
			if (el.hasClass("active")) {
				el.removeClass("active");
			} else {
				el.addClass("active");
			}
			return true;
		});
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
}
