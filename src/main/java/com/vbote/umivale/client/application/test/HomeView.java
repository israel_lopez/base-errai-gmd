 
package com.vbote.umivale.client.application.test;

/*
 * ${symbol_pound}%L
 * GwtMaterial
 * %%
 * Copyright (C) 2015 - 2016 GwtMaterialDesign
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ${symbol_pound}L%
 */


import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.ui.MaterialIcon;
import gwt.material.design.client.ui.MaterialLabel;
import gwt.material.design.client.ui.MaterialTextBox;
import gwt.material.design.client.ui.MaterialToast;

import org.jboss.errai.databinding.client.api.DataBinder;
import org.jboss.errai.databinding.client.api.StateSync;
import org.jboss.errai.ui.nav.client.local.DefaultPage;
import org.jboss.errai.ui.nav.client.local.Page;
import org.jboss.errai.ui.shared.api.annotations.AutoBound;
import org.jboss.errai.ui.shared.api.annotations.Bound;
import org.jboss.errai.ui.shared.api.annotations.DataField;
import org.jboss.errai.ui.shared.api.annotations.Model;
import org.jboss.errai.ui.shared.api.annotations.ModelSetter;
import org.jboss.errai.ui.shared.api.annotations.Templated;

import com.vbote.umivale.client.BaseView;
import com.vbote.umivale.client.model.WelcomeInfo;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;

@Templated
public class HomeView extends BaseView implements HomePresenter.Display {

    @Inject
    @DataField
    MaterialIcon icon;

    @Inject
    @DataField
    MaterialLabel label;

    @Inject
    @DataField
    @Bound
    MaterialTextBox message;
     
    
    @Inject
    @DataField
    @Bound
    MaterialTextBox id;
    	
    @Inject @Model
    WelcomeInfo model;
     

    @Override
    @ModelSetter
	public void setData(WelcomeInfo winfo) {
		//binder.setModel(winfo,StateSync.FROM_MODEL);
    		this.model=winfo;
	}
  
    @PostConstruct
    public void init() {
        icon.setIconType(IconType.POLYMER);
        label.setText("GWT Material Errai");
        //binder.bind(message, "message");
        //binder.bind(id, "id");
        
    }


}
