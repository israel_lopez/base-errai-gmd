package com.vbote.umivale.client.application.test;


import org.jboss.errai.ui.nav.client.local.Page;
import org.jboss.errai.ui.nav.client.local.PageHidden;
import org.jboss.errai.ui.nav.client.local.PageHiding;
import org.jboss.errai.ui.nav.client.local.PageShowing;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.vbote.umivale.client.BasePresenter;
import com.vbote.umivale.client.model.WelcomeInfo;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
@Page(path = "second")
public class SecondPresenter extends BasePresenter {

	public interface Display extends IsWidget {
		void setData(WelcomeInfo winfo);
		WelcomeInfo getData();
	}

	@Inject
	private Display display;
 
	
	@Override
	public Widget asWidget() {
		return display.asWidget();
	}

	@PageShowing
	public void prepare()
	{
		display.setData(persistenceManager.getWinfo());
		//display.setData(persistenceManager.getWelcomeMessage("SECOND"));
	}
	
	@PageHiding
	public void onPHiding()
	{
		//if (winfo!=null)
		//GWT.log("onHiding-->"+winfo.getMessage());
		// String l = display.getData().getMessage();
		// l.length();
	}
}
