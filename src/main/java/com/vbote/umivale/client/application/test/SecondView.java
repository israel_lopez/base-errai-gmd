package com.vbote.umivale.client.application.test;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.jboss.errai.databinding.client.api.DataBinder;
import org.jboss.errai.databinding.client.api.StateSync;
import org.jboss.errai.ui.shared.api.annotations.AutoBound;
import org.jboss.errai.ui.shared.api.annotations.Bound;
import org.jboss.errai.ui.shared.api.annotations.DataField;
import org.jboss.errai.ui.shared.api.annotations.Model;
import org.jboss.errai.ui.shared.api.annotations.ModelSetter;
import org.jboss.errai.ui.shared.api.annotations.Templated;

import com.google.gwt.user.client.ui.Composite;
import com.vbote.umivale.client.BaseView;
import com.vbote.umivale.client.model.WelcomeInfo;

import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.ui.MaterialIcon;
import gwt.material.design.client.ui.MaterialLabel;
import gwt.material.design.client.ui.MaterialTextBox;
 
@Templated
public class SecondView extends BaseView implements SecondPresenter.Display {

    @Inject
    @DataField
    MaterialIcon icon;

    @Inject
    @DataField
    MaterialLabel label;

    @Inject
    @DataField
    MaterialTextBox message;

    @Inject
    @DataField
    MaterialTextBox id;
    
    @Inject
    private DataBinder<WelcomeInfo> binder;

    @Override
	public void setData(WelcomeInfo winfo) {
		binder.setModel(winfo,StateSync.FROM_MODEL);
	}
     
    
    @PostConstruct
    public void init() {
        icon.setIconType(IconType.ACCESS_ALARM);
        label.setText("GWT Material Errai SECOND");
        binder.bind(message, "message");
        binder.bind(id, "id");
    }
 

	@Override
	public WelcomeInfo getData() {
		return binder.getModel();
	}


}
