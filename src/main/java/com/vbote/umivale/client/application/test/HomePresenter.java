package com.vbote.umivale.client.application.test;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jboss.errai.ui.nav.client.local.DefaultPage;
import org.jboss.errai.ui.nav.client.local.Page;
import org.jboss.errai.ui.nav.client.local.PageShowing;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.vbote.umivale.client.BasePresenter;
import com.vbote.umivale.client.model.WelcomeInfo;

@Singleton
@Page(path = "home", role = DefaultPage.class)
public class HomePresenter extends BasePresenter {
	public interface Display extends IsWidget {
		void setData(WelcomeInfo winfo);
	}

	@Inject
	private Display display;

	@Override
	public Widget asWidget() {
		return display.asWidget();
	}
	
	@PageShowing
	private void prepare()
	{
		display.setData(persistenceManager.getWinfo());
		//display.setData(persistenceManager.getWelcomeMessage("HOME"));
	}
}
