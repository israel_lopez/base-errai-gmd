 
package com.vbote.umivale.client.local.widget;

import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.user.client.ui.Composite;
import gwt.material.design.client.constants.NavBarType;
import gwt.material.design.client.ui.MaterialNavBar;
import gwt.material.design.client.ui.MaterialNavBrand;
import org.jboss.errai.ui.shared.api.annotations.DataField;
import org.jboss.errai.ui.shared.api.annotations.Templated;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Templated
public class Header extends Composite {

    @Inject
    @DataField
    MaterialNavBar navBar;

    @PostConstruct
    protected void init() {
        navBar.setActivates("sideNav");
        navBar.setLayoutPosition(Position.FIXED);
        navBar.setTop(0);
        navBar.add(new MaterialNavBrand("App Title"));
    }
}
