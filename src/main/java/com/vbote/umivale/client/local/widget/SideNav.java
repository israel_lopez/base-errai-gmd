 
package com.vbote.umivale.client.local.widget;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.ui.Composite;
import com.google.inject.Inject;
import com.vbote.umivale.client.events.PageChangeEvent;

import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.constants.SideNavType;
import gwt.material.design.client.ui.MaterialLink;
import gwt.material.design.client.ui.MaterialSideNav;
import gwt.material.design.client.ui.MaterialSideNavDrawer;
import gwt.material.design.client.ui.MaterialSideNavPush;

import org.jboss.errai.ui.shared.api.annotations.DataField;
import org.jboss.errai.ui.shared.api.annotations.Templated;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Observes;

@Templated
public class SideNav extends Composite {

    @Inject
    @DataField
    MaterialSideNavDrawer sideNav;

    @PostConstruct
    protected void init() {
        sideNav.setId("sideNav");
        sideNav.setWithHeader(true);
        sideNav.setCloseOnClick(true);
        sideNav.setWidth(280);
        sideNav.reload();
        for (int i = 1; i <= 3; i++) {
            MaterialLink link = new MaterialLink("Item " + i);
            link.setIconType(IconType.POLYMER);
            if (i==2) 
            		link.setHref("#second");
            else
            		link.setHref("#home");
            	
            sideNav.add(link);
        }
    }
    
    public void onPageChange(@Observes PageChangeEvent event) {
		GWT.log("onPageChange:"+event.getPageId());
    }
}
