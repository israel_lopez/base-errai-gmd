package com.vbote.umivale.client.events;

public class PageChangeEvent {

	String pageId;

	public PageChangeEvent(String pageId) {
		super();
		this.pageId = pageId;
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
}
