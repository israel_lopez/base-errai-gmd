package com.vbote.umivale.client.services;

import javax.enterprise.inject.Produces;
import javax.inject.Singleton;

import com.vbote.umivale.client.model.WelcomeInfo;

@Singleton
public class PersistenceManager {
	
	 
	WelcomeInfo winfo = new WelcomeInfo();
	
	public PersistenceManager()
	{
		winfo.setId("FROMPM");
	}
	public WelcomeInfo getWelcomeMessage(String from)
	{
		
		winfo.setMessage("Welcome ERRAI - "+ from+ "!");
		return winfo;
	}
	
	public WelcomeInfo getWinfo() {
		return winfo;
	}

}
