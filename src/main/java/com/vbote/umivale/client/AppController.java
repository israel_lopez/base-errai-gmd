package com.vbote.umivale.client;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.jboss.errai.ui.nav.client.local.Navigation;

import com.google.gwt.dom.client.StyleInjector;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Panel;
import com.vbote.umivale.client.resources.AppClientBundle;
import com.vbote.umivale.client.local.widget.Header;
import com.vbote.umivale.client.local.widget.Main;
import com.vbote.umivale.client.local.widget.SideNav;

public class AppController extends Composite {

    @Inject
    Navigation navigation;

    @Inject
    Header header;

    @Inject
    SideNav sideNav;

    @Inject
    Main content;

    @PostConstruct
    public void init() {
        StyleInjector.inject(AppClientBundle.INSTANCE.appCss().getText());
        content.getContainer().add(navigation.getContentPanel());
    }
    
    public void go(Panel parent)
    {
    		parent.add(header);
    		parent.add(sideNav);
    		parent.add(content);
    }
}
